package com.ynov.blogapi.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ynov.blogapi.model.Article;
import com.ynov.blogapi.model.Category;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Integer> {
	
	//Récupérer une Categrorie grâce à son titre - requête dérivées
	//public Iterable<Category> findByTitle(String title);

}