package com.ynov.blogapi.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ynov.blogapi.model.Article;

@Repository
public interface ArticleRepository extends CrudRepository<Article, Integer>{
	
	//Récupérer un article grâce à son titre - requête dérivées
	public Iterable<Article> findByTitle(String title);
	
//	//Récupérer un article grâce à son id - requête dérivées
//	public Optional<Article> findById(Integer id);
	
}
