package com.ynov.blogapi.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ynov.blogapi.model.Comment;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Integer> {

}
