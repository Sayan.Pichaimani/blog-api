package com.ynov.blogapi.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ynov.blogapi.model.InternalUser;

@Repository
public interface InternalUserRepository extends CrudRepository<InternalUser, Integer>{
	
	public InternalUser findByUsername(String username);

}
