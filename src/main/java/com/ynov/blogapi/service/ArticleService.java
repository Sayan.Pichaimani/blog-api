package com.ynov.blogapi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ynov.blogapi.exceptions.NotFoundException;
import com.ynov.blogapi.model.Article;
import com.ynov.blogapi.repository.ArticleRepository;
import com.ynov.blogapi.transformer.article.ArticleFull;
import com.ynov.blogapi.transformer.article.ArticleTransformer;
@Service
public class ArticleService {

	@Autowired
	private ArticleRepository articleRepository;
	
	@Autowired
	private ArticleTransformer articleTransformer;
		
	
	public ArticleFull upsert(Article article) {
		return articleTransformer.transform(articleRepository.save(article));
	}
	
	public ArticleFull upsert(ArticleFull article) {
		return upsert(articleTransformer.untransform(article));
	}
	
	public ArticleFull getArticle(Integer id) throws NotFoundException {
		Optional<Article> res = articleRepository.findById(id);
		if (res.isPresent()) {
			return articleTransformer.transform(res.get());
		} else {
			throw new NotFoundException();
		}
	}
	
	public Optional<Article> getEntityArticle(Integer id) {
		return articleRepository.findById(id);
	}
	
	public List<ArticleFull> getArticles(){
		return articleTransformer.transform(articleRepository.findAll());
	}
	
	public void deleteArticle(Integer id) {
		articleRepository.deleteById(id);
	}
	
	public Iterable<Article> getArticleByTitle(String title){
		return articleRepository.findByTitle(title);
	}
	
	
}
