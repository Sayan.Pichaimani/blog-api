package com.ynov.blogapi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ynov.blogapi.model.Comment;
import com.ynov.blogapi.repository.CommentRepository;
import com.ynov.blogapi.transformer.comment.CommentFull;
import com.ynov.blogapi.transformer.comment.CommentTransformer;

@Service
public class CommentService {

	@Autowired
	private CommentTransformer commentTransformer;
	
	@Autowired	
	private CommentRepository commentRepository;

	public List<CommentFull> getComments(){
		return commentTransformer.transform(commentRepository.findAll());
	}
	
	public void deleteComment(int id) {
		commentRepository.deleteById(id);
	}

	public Optional<Comment> getComment(Integer id_comment) {
		return commentRepository.findById(id_comment);
	}
}
