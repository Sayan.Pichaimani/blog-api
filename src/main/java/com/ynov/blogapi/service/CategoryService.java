package com.ynov.blogapi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ynov.blogapi.exceptions.NotFoundException;
import com.ynov.blogapi.model.Article;
import com.ynov.blogapi.model.Category;
import com.ynov.blogapi.repository.CategoryRepository;
import com.ynov.blogapi.transformer.article.ArticleFull;
import com.ynov.blogapi.transformer.category.CategoryFull;
import com.ynov.blogapi.transformer.category.CategoryTransformer;

@Service
public class CategoryService {
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private CategoryTransformer categoryTransformer;
	
	public Category upsert(Category category) {
		return categoryRepository.save(category);
	}
	
	public CategoryFull getCategory(Integer id) throws NotFoundException {
		Optional<Category> res = categoryRepository.findById(id);
		if (res.isPresent()) {
			return categoryTransformer.transform(res.get());
		} else {
			throw new NotFoundException();
		}
	}
	
	public List<CategoryFull> getCategories() {
		return categoryTransformer.transform(categoryRepository.findAll());
	}
	
	public void deleteCategory(Integer id) {
		categoryRepository.deleteById(id);
	}

}
