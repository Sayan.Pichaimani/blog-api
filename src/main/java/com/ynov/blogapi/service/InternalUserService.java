package com.ynov.blogapi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ynov.blogapi.model.InternalUser;
import com.ynov.blogapi.repository.InternalUserRepository;

@Service
public class InternalUserService {

	@Autowired
	private InternalUserRepository internalUserRepository;
		
	public InternalUser upsert(InternalUser internalUser) {
		return internalUserRepository.save(internalUser);
	}
	
	public List<InternalUser> getInternalUsers(){
		return (List<InternalUser>) internalUserRepository.findAll();
	}
	
	public Optional<InternalUser> getInternalUser(Integer id){
		return internalUserRepository.findById(id);
	}
	
	public InternalUser getUserByUsername(String username) {
		return internalUserRepository.findByUsername(username);
	}
		
	public void deleteInternalUser(Integer id) {
		internalUserRepository.deleteById(id);
	}
	
}
