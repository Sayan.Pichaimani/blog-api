package com.ynov.blogapi.transformer.comment;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.ynov.blogapi.model.Comment;

@Component
public class CommentTransformer {

	
public CommentFull transform(Comment comment) {
		
		CommentFull commentFull = new CommentFull();
		
		commentFull.setId(comment.getId());
		commentFull.setText(comment.getText());
		commentFull.setDate(comment.getDate());		
		
		commentFull.setUsers(comment.getUser());
		

//		for (ArticleLight articlelight : comment) {
//			ArticleLight articleLight = new ArticleLight();
//			articleLight.setId(articleLight.getId());
//			articleLight.setTitle(articleLight.getTitle());
//			articleLight.setDate(articleLight.getDate());
//			articleLight.setContent(articleLight.getContent());
//			commentFull.getArticles().add(articleLight);
//		}
//		
		return commentFull;
	}
	
	public List<CommentFull> transform(Iterable<Comment> comments){
		List<CommentFull> commentFull = new ArrayList<>();
		for(Comment comment : comments) {
			commentFull.add(transform(comment));
		}
		return commentFull;
	}


	public CommentLight untransform(CommentLight comment) {
		CommentLight c = new CommentLight();
		
		c.setId(comment.getId());
		c.setText(comment.getText());
		c.setDate(comment.getDate());
		
		return c;
	}
}
