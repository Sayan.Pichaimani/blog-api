package com.ynov.blogapi.transformer.comment;

import java.util.ArrayList;
import java.util.List;

import com.ynov.blogapi.model.Article;
import com.ynov.blogapi.model.InternalUser;
import com.ynov.blogapi.transformer.article.ArticleLight;

public class CommentFull extends CommentLight{

//	private List<InternalUser> users = new ArrayList<>();
//	
	private List<ArticleLight> articles = new ArrayList<>();
//
//	public List<InternalUser> getUsers() {
//		return users;
//	}
//	
//	public void setUsers(List<InternalUser> users) {
//		this.users = users;
//	}
//
	public List<ArticleLight> getArticles() {
		return articles;
	}

	public void setArticles(List<ArticleLight> articles) {
		this.articles = articles;
	}
	
	private InternalUser users = new InternalUser();
	
//	private ArticleLight articles = new ArticleLight();

	public InternalUser getUsers() {
		return users;
	}

	public void setUsers(InternalUser users) {
		this.users = users;
	}

//	public ArticleLight getArticles() {
//		return articles;
//	}
//
//	public void setArticles(ArticleLight articles) {
//		this.articles = articles;
//	}
	
}
