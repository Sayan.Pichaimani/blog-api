package com.ynov.blogapi.transformer.comment;

import java.util.Date;

public class CommentLight {
	
	protected Integer id;
	protected String text;
	protected Date date;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	

	
}
