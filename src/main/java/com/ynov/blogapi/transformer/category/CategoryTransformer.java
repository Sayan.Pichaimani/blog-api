package com.ynov.blogapi.transformer.category;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.ynov.blogapi.model.Article;
import com.ynov.blogapi.model.Category;
import com.ynov.blogapi.model.Comment;
import com.ynov.blogapi.transformer.article.ArticleLight;
import com.ynov.blogapi.transformer.comment.CommentLight;

@Component
public class CategoryTransformer {
	
	public CategoryFull transform(Category category) {
		CategoryFull categoryFull = new CategoryFull();
		
		categoryFull.setId(category.getId());
		categoryFull.setName(category.getName());
		
//		categoryFull.setArticles(category.getArticles());
		
		for(Article article : category.getArticles()) {
			ArticleLight articleLight = new ArticleLight();
			
			articleLight.setId(article.getId());
			articleLight.setTitle(article.getTitle());
			articleLight.setDate(article.getDate());
			articleLight.setContent(article.getContent());
			
			categoryFull.getArticles().add(articleLight);
		}
		
		return categoryFull;
	}
	
	public List<CategoryFull> transform(Iterable<Category> categories){
		List<CategoryFull> categoriesFull = new ArrayList<>();
		for(Category category : categories) {
			categoriesFull.add(transform(category));
		}
		return categoriesFull;
	}

}
