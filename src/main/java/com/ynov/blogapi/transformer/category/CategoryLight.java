package com.ynov.blogapi.transformer.category;

public class CategoryLight {
	
	protected Integer id;
	protected String name;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
