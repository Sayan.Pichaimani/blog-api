package com.ynov.blogapi.transformer.article;

import java.util.ArrayList;
import java.util.List;

import com.ynov.blogapi.transformer.category.CategoryLight;
import com.ynov.blogapi.transformer.comment.CommentLight;

public class ArticleFull extends ArticleLight {

	private List<CommentLight> comments = new ArrayList<>();
	
//	private List<CategoryLight> categories = new ArrayList<>();

	public List<CommentLight> getComments() {
		return comments;
	}
	public void setComments(List<CommentLight> comments) {
		this.comments = comments;
	}
	
//	public List<CategoryLight> getCategories() {
//		return categories;
//	}
//	public void setCategories(List<CategoryLight> categories) {
//		this.categories = categories;
//	}
}
