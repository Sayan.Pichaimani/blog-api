package com.ynov.blogapi.transformer.article;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.ynov.blogapi.model.Article;
import com.ynov.blogapi.model.Category;
import com.ynov.blogapi.model.Comment;
import com.ynov.blogapi.transformer.category.CategoryLight;
import com.ynov.blogapi.transformer.comment.CommentLight;



@Component
public class ArticleTransformer {

	public ArticleFull transform(Article article) {
		
		ArticleFull articleFull = new ArticleFull();
		
		articleFull.setId(article.getId());
		articleFull.setTitle(article.getTitle());
		articleFull.setDate(article.getDate());
		articleFull.setContent(article.getContent());
		
		for(Comment comment : article.getComments()) {
			CommentLight commentLight = new CommentLight();
			
			commentLight.setId(comment.getId());
			commentLight.setText(comment.getText());
			commentLight.setDate(comment.getDate());
			
			articleFull.getComments().add(commentLight);
		}	
		
//		for(Category category : article.getCategories()) {
//			CategoryLight categoryLight = new CategoryLight();
//			
//			categoryLight.setId(category.getId());
//			categoryLight.setName(category.getName());
//			
//			articleFull.getCategories().add(categoryLight);
//		}
		
		
		
		return articleFull;
	}
	
	public List<ArticleFull> transform(Iterable<Article> articles){
		List<ArticleFull> articleFull = new ArrayList<>();
		for(Article article : articles) {
			articleFull.add(transform(article));
		}
		return articleFull;
	}
	
	public Article untransform(ArticleFull article) {
		Article p = new Article();
		p.setId(article.getId());
		p.setTitle(article.getTitle());
		p.setDate(article.getDate());
		p.setContent(article.getContent());

		return p;
	}
	
}
