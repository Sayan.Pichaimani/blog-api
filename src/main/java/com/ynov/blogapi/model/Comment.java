package com.ynov.blogapi.model;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "comments")
public class Comment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private Integer id;
	private String text;
	private Date date;
	
	@OneToOne(
			cascade = CascadeType.ALL
			)
	@JoinColumn(name = "user_id")
	private InternalUser user = new InternalUser();
	
	@OneToOne(
			cascade = CascadeType.ALL
			)
	@JoinColumn(name = "article_id")
	private Article article = new Article();
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public InternalUser getUser() {
		return user;
	}
	public void setUser(InternalUser user) {
		this.user = user;
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	
	
}
 	