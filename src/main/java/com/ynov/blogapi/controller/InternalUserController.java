package com.ynov.blogapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ynov.blogapi.model.InternalUser;
import com.ynov.blogapi.service.InternalUserService;

@RestController
@RequestMapping("/api/private")
public class InternalUserController {

	//Injection de InternalUser service
	@Autowired
	private InternalUserService internalUserService;
	
	//Récupérer la liste des utilisateurs
	@GetMapping("/users")
	public List<InternalUser> getInternalUsers(){
		return internalUserService.getInternalUsers();
	}
	
	//Récupérer un utilisateur grâce à son id
	@GetMapping("/user/{id}")
	public ResponseEntity<InternalUser> getInternalUser(@PathVariable("id") Integer id){
		Optional<InternalUser> internalUser = internalUserService.getInternalUser(id);
		if(internalUser.isPresent()) {
			return new ResponseEntity<InternalUser>(internalUser.get(), HttpStatus.OK);
		}
		return new ResponseEntity<InternalUser>(HttpStatus.NOT_FOUND);
	}
	
	//Pas de méthode Article (create) - la création est gérée lors de l'inscription
	
	//Modification d'un user
	@PatchMapping("/user")
	public ResponseEntity<InternalUser> partialReplaceInternalUser(@RequestBody InternalUser internalUser){
		Optional<InternalUser> iu = internalUserService.getInternalUser(internalUser.getId());
		if(iu.isPresent()) {
			InternalUser existingInternalUser = iu.get();
			
			if(internalUser.getUsername() != null == !internalUser.getUsername().equals(existingInternalUser.getUsername())){
				existingInternalUser.setUsername(internalUser.getUsername());
			}
			
			if(internalUser.getPassword() != null == !internalUser.getPassword().equals(existingInternalUser.getPassword())) {
				existingInternalUser.setPassword(internalUser.getPassword());
			}
			
			existingInternalUser = internalUserService.upsert(existingInternalUser);
			return new ResponseEntity<InternalUser>(existingInternalUser, HttpStatus.OK);
		}
		return new ResponseEntity<InternalUser>(HttpStatus.NOT_FOUND);
	}
}
