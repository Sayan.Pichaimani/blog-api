package com.ynov.blogapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ynov.blogapi.configuration.JwtTokenUtil;
import com.ynov.blogapi.exceptions.NotFoundException;
import com.ynov.blogapi.model.Article;
import com.ynov.blogapi.model.Category;
import com.ynov.blogapi.model.InternalUser;
import com.ynov.blogapi.service.ArticleService;
import com.ynov.blogapi.service.CategoryService;
import com.ynov.blogapi.transformer.article.ArticleFull;
import com.ynov.blogapi.transformer.category.CategoryFull;

@RestController
@RequestMapping("api/public")
public class PublicController {
	
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ArticleService articleService;
	
	// Se connecter
	@PostMapping("/login")
	public ResponseEntity<String> login(
			@RequestBody InternalUser user){
		try {
			Authentication authenticate =
					authenticationManager.authenticate(
							new UsernamePasswordAuthenticationToken(
									user.getUsername(),
									user.getPassword()
									)
							);
			User authenticatedUser = (User) authenticate.getPrincipal();
			String token =
					jwtTokenUtil.generateAccessToken(authenticatedUser);
			System.out.println("Token is : " + token);
			
			String text = authenticatedUser.getUsername()
					+ " successfully authenticated";
			ResponseEntity<String> response =
					ResponseEntity.ok()
					.header(HttpHeaders.AUTHORIZATION, token)
					.body(text);
			return response;
		} catch (BadCredentialsException e) {
			return ResponseEntity.status(
					HttpStatus.UNAUTHORIZED).build();
					
		}
	}
	
	@GetMapping("/securitynone")
	public String securityNone() {
		return "open";
	}
	
	// Visualiser liste des catégories
	@GetMapping("/categories")
	public List<CategoryFull> getCategories() {
		return categoryService.getCategories();
	}

	//visualiser 1 article

	@GetMapping("/article/{id}")
	public ResponseEntity<ArticleFull> getArticle(@PathVariable("id") Integer id) {
		try {
			ArticleFull p = articleService.getArticle(id);
			return new ResponseEntity<ArticleFull>(p, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<ArticleFull>(HttpStatus.NOT_FOUND);
		}
	}
}
