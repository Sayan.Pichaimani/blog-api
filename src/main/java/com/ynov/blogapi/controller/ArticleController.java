package com.ynov.blogapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ynov.blogapi.exceptions.NotFoundException;
import com.ynov.blogapi.model.Article;
import com.ynov.blogapi.service.ArticleService;
import com.ynov.blogapi.transformer.article.ArticleFull;


@RestController
@RequestMapping("/api/private")
public class ArticleController {

	//Injection de Article service
	@Autowired
	private ArticleService articleService;
	
	//Récupérer tous les articles
	@GetMapping("/articles")
	public List<ArticleFull> getArticles(){
		return articleService.getArticles();
	}
	
	//Récupération d'un article grâce à son ID
	@GetMapping("/article/{id}")
	public ResponseEntity<ArticleFull> getArticle(@PathVariable("id") Integer id) {
		try {
			ArticleFull a = articleService.getArticle(id);
			return new ResponseEntity<ArticleFull>(a, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<ArticleFull>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping("/article/{id}")
	public void deleteArticle(@PathVariable("id") Integer id) {
		articleService.deleteArticle(id);
	}
	
	//Création d'un article
	@PostMapping("/article")
	public ArticleFull addArticle(@RequestBody Article article) {
		return articleService.upsert(article);
	}
	
	@PutMapping("/article")
	public ArticleFull replaceArticle(@RequestBody Article article) {
		return articleService.upsert(article);
	}
	
	//Récupérer un article grâce à son titre
	@GetMapping("/article/filter/{title}")
	public Iterable<Article> getArticleByTitle(@PathVariable("name") String title){
		return articleService.getArticleByTitle(title);
	}
	
}
