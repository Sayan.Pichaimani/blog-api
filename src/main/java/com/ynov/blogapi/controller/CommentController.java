package com.ynov.blogapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ynov.blogapi.service.CommentService;
import com.ynov.blogapi.transformer.comment.CommentFull;

@RestController
@RequestMapping("api/private")
public class CommentController {

	@Autowired
	private CommentService commentService;
	
	//Récupérer tous les articles
	@GetMapping("/comments")
	public List<CommentFull> getComments(){
		return commentService.getComments();
	}
}
