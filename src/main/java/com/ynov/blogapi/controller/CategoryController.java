package com.ynov.blogapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ynov.blogapi.exceptions.NotFoundException;
import com.ynov.blogapi.model.Category;
import com.ynov.blogapi.service.CategoryService;
import com.ynov.blogapi.transformer.article.ArticleFull;
import com.ynov.blogapi.transformer.category.CategoryFull;

@RestController
@RequestMapping("api/private")
public class CategoryController {

	//Injection de Category service
	@Autowired
	private CategoryService categoryService;
	
	//Récupérer toutes les catégories
	@GetMapping("/categories")
	public List<CategoryFull> getCategories() {
		return categoryService.getCategories();
	}
	
	//Récupération d'un article grâce à son ID
		@GetMapping("/category/{id}")
		public ResponseEntity<CategoryFull> getArticle(@PathVariable("id") Integer id) {
			try {
				CategoryFull c = categoryService.getCategory(id);
				return new ResponseEntity<CategoryFull>(c, HttpStatus.OK);
			} catch (NotFoundException e) {
				return new ResponseEntity<CategoryFull>(HttpStatus.NOT_FOUND);
			}
		}
	
	// Création d'une catégorie
	@PostMapping("/category")
	public Category addCategory(@RequestBody Category category) {
		return categoryService.upsert(category);
	}
}
