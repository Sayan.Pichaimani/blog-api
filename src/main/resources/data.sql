DROP TABLE IF EXISTS articles;
DROP TABLE IF EXISTS comments;
DROP TABLE IF EXISTS categories;
DROP TABLE IF EXISTS users;
 
 
 CREATE TABLE categories (
  category_id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250)
);

INSERT INTO categories (name) VALUES
  ('Basketball'),
  ('Football'),
  ('Handball'); 
 
CREATE TABLE articles(
  article_id INT AUTO_INCREMENT  PRIMARY KEY,
  title VARCHAR(250),
  date TIMESTAMP,
  content VARCHAR(250),
  user_id INT NOT NULL
);
 
INSERT INTO articles(title,date,content, user_id) VALUES
  ('Blessure : Mbappe','2020-10-21 10:00:00','Kylian Mbappe sest blessé a la cheville face à Lille', 1),
  ('Résultat : Lakers','2021-01-31 05:36:00', 'Les Lakers emportent la victoire face aux Suns', 1),
  ('Qualification : France','2013-03-17 23:00:00', 'La France se qualifie pour le mondial 2014', 2);
  

CREATE TABLE category_article (
	category_id INT NOT NULL,
	article_id INT NOT NULL
);

INSERT INTO category_article (category_id, article_id) VALUES
	(1,2),
	(2,1),
	(3,3),
	(3,1);
	
    
 CREATE TABLE comments (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  text VARCHAR(250) NOT NULL,
  date TIMESTAMP NOT NULL,
  user_id INT,
  article_id INT
);

INSERT INTO comments (text, date, user_id, article_id) VALUES
  ('Coup dur pour le PSG', '2020-10-21 15:00:00', 2, 1),
  ('Go Lakers !', '2021-01-31 10:15:00',2 , 2),
  ('Trop forte cette équipe', '2021-01-31 07:15:00', 1, 2);
  

CREATE TABLE users(
	id INT AUTO_INCREMENT  PRIMARY KEY,
	username VARCHAR(255),
	password VARCHAR(255)
);

INSERT INTO users(username, password) VALUES
	('user',
	'$2a$12$3QJUKRUlH6eOldgAeEhBXuVXnFYcdG9L6TeJ8Dtw726lmYrFPomgW'),
	('admin',
	'$2y$10$5eFxNpYOF7sAByFkxYe11.VQfIrL7aNbTjWtKCzSI9d6IYd92tXzS');
 